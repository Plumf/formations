| [Accueil - Support Papier](/contenus-papiers/README.md) | [Accueil Formation @Plumf](/README.md) | [https://plumf.eu](https://plumf.eu) |
|---------------------------------------------------------|----------------------------------------|--------------------------------------|

# Ressources autour du papier

## Template et fichier pdf
| Nom du template | Ressources | Source | Niveau difficulté |
|-----------------|------------|--------|-------------------|
| Café Noir | [origami-cafe_noir.pdf](contenus-papiers/Ressources/Modeles%20Origamis/origami-cafe_noir.pdf) | http://aureleduda.com/telechargement/260/ | *** |

## Ressources Internet
+ [Wikipedia.org - origami](https://fr.wikipedia.org/wiki/Origami)
+ [Mouvement Français des Plieurs de Papier](https://mfpp-origami.fr)
+ [Origami autour des enveloppes et des lettres - ghh.com](https://ghh.com)
+ [Forum francophone du pliage de papier (https://pliagedepapier.com)](https://pliagedepapier.com/forum/)
+ [Site de Jo Nakashima (Vidéos tutoriels d'Origami)](https://jonakashima.com.br)

## Ressources documentaire
+ [Magazine *LE PLI* specimen](/contenus-papiers/Ressources/Sp%C3%A9cimen-LE-PLI-2014.pdf) du Mouvement français des plieurs de papier [Page des archives du PLI](https://mfpp-origami.fr/le-pli/)