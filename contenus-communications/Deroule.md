![](/contenus-communications/img/cemea_logo.png) License Creative Commons Share-Alike 4.0 International


# Déroulé Intervention Communication Externe

| Journée 1 | Contenus | Commentaire |
|---------|---------|----------|
| 09:00-10:00 | Une image vaut parfois plus que des mots | Premier travail sur l'image : les pictogrammes |
| 09:00-09:30 | Jeu Concept | Faire découvrir des mots et expressions au travers de pictogrammes |
| 09:30-10:00 | Analyse des différents degrés de codages des pictogrammes | |
| 10:00-10:20 | Explorer les différents outils visuels de communication au sein du lieu d'accueil | Restitution et analyse |
| 10:30-11:30 | Analyse de la forme et du fond des différents supports de communication (affiches, flyers, plaquettes)   Analyse des affiches Greenpeace   Mise en page, choix des illustrations, des couleurs, des logos, de la charte graphique | Approche de divers concepts : la dénotation, la connotation, la métaphore visuelle   Partage et analyse des supports de communication ramenés par les stagiaires de leur structure |
| 11/30-12:00 | Eléments théoriques sur la construction d’un affiche : son découpage, l’agencement des textes et polices, sa grille de lecture, logos et charte graphique, la palette des couleurs, la hiérarchie des messages.
| 12:00-12:30 | Les relations avec les médias (presse, radio, télévision, réseaux sociaux) | Rédaction mail, note interne,  communiqué de presse |
| 13:30-14:00 | Le blason de la communication | |
| 14:00-14:30 | Le plan de communication | |
| 14:30-15:15 | Utilisation du Kit-boxing pour la création d’affiches « Gala de boxe » | Mise en pratique de la construction d’affiches en sous-groupes |
| 15:30-15:45 | | Présentation des affiches, échanges sur les choix réalisés |
| 15:45-16:15 | Communiquer autour de son projet et de ses activités pour accrocher et sensibiliser publics et partenaires / Recherche de partenaires | 
| 16:15-16:45 | Apports législatifs : droits à l’image, droits des biens matériels, loi sur les impressions. |
| 16:45-17:00 | Bilan rapide de la première journée et présentation de la journée suivante. | Axée sur les outils numériques dans la communication (3h30) et la présentation et l’utilisation de logiciels (3h30). | |